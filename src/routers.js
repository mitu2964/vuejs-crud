import HomePage from './components/HomePage'
import SignUp from './components/SignUp'
import LoginPage from './components/LoginPage'
import Create from './components/Restaurant/Create'
import Index from './components/Restaurant/Index'
import Edit from './components/Restaurant/Edit'
import View from './components/Restaurant/View'
import PageNotFound from './components/PageNotFound'
import { createRouter, createWebHistory  } from 'vue-router'

const routes = [
    {
        name:'HomePage',
        component: HomePage,
        path:'/'
    },
    {
        name:'Signup',
        component: SignUp,
        path:'/sign-up'
    },
    {
        name:'LoginPage',
        component: LoginPage,
        path:'/login'
    },
    {
        name:'Create',
        component: Create,
        path:'/restaurants/create'
    },
    {
        name:'Edit',
        component: Edit,
        path:'/restaurants/edit/:id'
    },
    {
        name:'View',
        component: View,
        path:'/restaurants/view/:id'
    },
    {
        name:'Index',
        component: Index,
        path:'/restaurants'
    },
    { path: '/:pathMatch(.*)*', component: PageNotFound }
];

const router = createRouter({
    history: createWebHistory(),
    routes
});

export default router;

